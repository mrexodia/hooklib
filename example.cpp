#include "hooklib.h"

HOOK hMessageBoxA;

int WINAPI MessageBoxA_hook(HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType)
{
    unhook(hMessageBoxA);
    int ret=MessageBoxA(hWnd, lpText, "Hooked!", uType);
    hook(hMessageBoxA);
    return ret;
}

int main(int argc, char* argv[])
{
    hMessageBoxA=hook((void*)MessageBoxA, (void*)MessageBoxA_hook);
    MessageBoxA(0, "This is a simple test message.", "Test", MB_ICONINFORMATION);
    return 0;
}
