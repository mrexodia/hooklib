#include "hooklib.h"

static HOOK hook_internal(ULONG_PTR addr, void* newfunc)
{
    HOOK hook=(HOOK)malloc(sizeof(hookstruct));
    hook->addr=addr;
#ifdef _WIN64
    hook->hook.mov=0xB848;
#else
    hook->hook.mov=0xB8;
#endif
    hook->hook.addr=(ULONG_PTR)newfunc;
    hook->hook.push=0x50;
    hook->hook.ret=0xc3;
    memcpy(&hook->orig, (const void*)addr, sizeof(opcode));
    if(!WriteProcessMemory(GetCurrentProcess(), (void*)addr, &hook->hook, sizeof(opcode), 0))
    {
        free(hook);
        return 0;
    }
    return hook;
}

HOOK hook(void* api, void* newfunc)
{
    if(!api)
        return 0;
    return hook_internal((ULONG_PTR)api, newfunc);
}

bool unhook(HOOK hook, bool freehook)
{
    if(hook && hook->addr && WriteProcessMemory(GetCurrentProcess(), (void*)hook->addr, hook->orig, sizeof(opcode), 0))
    {
        if(freehook)
            free(hook);
        return true;
    }
    return false;
}

bool unhook(HOOK hook)
{
    return unhook(hook, false);
}

bool hook(HOOK hook)
{
    if(!hook)
        return false;
    return WriteProcessMemory(GetCurrentProcess(), (void*)hook->addr, &hook->hook, sizeof(opcode), 0);
}
